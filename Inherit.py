class details:
    type='Student'
    def __init__(self):
        print('Parent Class')
    def info(self,name,roll_no):
        # return name+str(roll_no)
        print('Name: {} Roll No: {}'.format(name,roll_no))

class fee(details):
    print('Child Class')
    def total(self,tuition,academic):
        print(tuition+academic)

obj=fee()
obj.total(1000,2000)
obj.info('aman',1)
print(obj.type)

pobj=details()
pobj.info('Peter',100)
pobj.total(10,10)                   #produce error,cz parent can't access child properties but child can access parent class properties