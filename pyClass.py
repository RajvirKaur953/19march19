class person:
    category='human'
    # def get_name(self,first,last):
    def set_name(self,first,last):
        # return first+last
        self.f=first
        self.l=last

    def printname(self):
        # return "First Name:{} Last Name:{}".format(self.first,self.last)
        return "First Name:{} Last Name:{} Category:{}".format(self.f,self.l,self.category)

p1=person()
# print(type(p1))                         #<class '__main__.person'>object of person class
# print(p1.category)
# print(p1.__class__.category)
# print(p1.get_name('Peter','Parker'))
# print(p1.printname())

p1.set_name('Rajvir','Kaur')
print(p1.printname())


p2=person()
p2.set_name('Peter','Parker')
print(p2.printname())
