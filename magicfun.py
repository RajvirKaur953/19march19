class circle:
    pi=3.14

    def __init__(self,r):                   #constructor
        # pass
        self.r=r
        print('Values Initialized')
    
    # def radius(self):
    #     # pass
    #     return self.pi*self.r*self.r

    def __str__(self):                          #magic function ,str=string representation ,
        print('Called')
        print(self.pi*self.r*self.r)

    def __del__(self):                          #destructor never calls it
        print('Del Called Values',self.r)
        del self.r
        print('Values Destroyed')
        print(self.r)
o=circle(10)
# print(o.radius())
o.__str__()                                 #neither constructor nor destructor, its a magic function
# o.__class__.r()